import React from 'react'
import {Container, Row, Col} from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './fontLibrary.js'


const social = "col d-flex justify-content-center";
const socialLinks = [
    {
        name: "facebook",
        href: "https://facebook.com/blackbreadgonemad",
        icon: ['fab','facebook-f']
    },
    {
        name: "youtube",
        href: "https://www.youtube.com/channel/UC2JwuZFVa_CloxgFjd7HGpQ",
        icon: ['fab','youtube']
    },
    {
        name: "instagram",
        href: "https://www.instagram.com/blackbreadgonemad/",
        icon: ['fab','instagram']
    },
    {
        name: "soundcloud",
        href: "https://soundcloud.com/blackbreadgonemad",
        icon: ['fab','soundcloud']
    },
    {
        name: "spotify",
        href: "https://open.spotify.com/artist/1xGCdUjcoFCu9tosmcBMgL",
        icon: ['fab','spotify']
    },
]

const Socials = (props) => (
    <Container className="d-flex justify-content-center pl-4">
        <Row style={props.style} >
            {socialLinks.map( socialLink => (
                <Col key={socialLink.name} className={social}>
                    <a
                        href={socialLink.href}
                        target="_blank"
                        rel="noopener noreferrer"
                        className={props.className}
                    >
                        <FontAwesomeIcon icon={socialLink.icon} size="sm"/>
                    </a>
                </Col>
            ))}
        </Row>
    </Container>
)

export default Socials;