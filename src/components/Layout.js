import React from "react"
import PropTypes from "prop-types"

import Footer from "./Footer"
import HeaderNav from "./HeaderNav"

import 'bootstrap/dist/css/bootstrap.min.css';

import '../styles/main.css'


const Layout = ({ children }) => {

  return (

    <>
      <div className="wrap bg-light">
        <HeaderNav className="header"/>
        <main className="container clear-top d-flex flex-row justify-content-center align-items-center ">
          {children}
        </main>
      </div>
      <Footer className="footer" />
      
    </>
    
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
