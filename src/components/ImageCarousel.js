import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
import Img from "gatsby-image"
import {useStaticQuery, graphql } from 'gatsby'



const ImageCarousel = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const data = useStaticQuery(graphql`
    query AboutImageCarousel {
        CarouselImages: allFile(filter: {relativeDirectory: {eq: "menu"}}) {
            nodes {
                childImageSharp {
                    id 
                    fluid{
                        ...GatsbyImageSharpFluid
                    }
                }
                name
            }
        }
    }
`)

const items = [
    {
      fluid: data.CarouselImages.nodes[0].childImageSharp.fluid,
      altText: 'Slide 1',
      captionHeader: 'Lee',
      id: data.CarouselImages.nodes[0].childImageSharp.id
    },
    {
      fluid: data.CarouselImages.nodes[1].childImageSharp.fluid,
      altText: 'Slide 2',
      captionHeader: 'Martin',
      id: data.CarouselImages.nodes[1].childImageSharp.id
    },
    {
      fluid: data.CarouselImages.nodes[2].childImageSharp.fluid,
      altText: 'Slide 3',
      captionHeader: 'Mati',
      id: data.CarouselImages.nodes[2].childImageSharp.id
    },
    {
      fluid: data.CarouselImages.nodes[3].childImageSharp.fluid,
      altText: 'Slide 4',
      captionHeader: 'Merike',
      id: data.CarouselImages.nodes[3].childImageSharp.id
    },
    {
      fluid: data.CarouselImages.nodes[4].childImageSharp.fluid,
      altText: 'Slide 4',
      captionHeader: 'Peeter',
      id: data.CarouselImages.nodes[4].childImageSharp.id
    },
  ];

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.id}
      >
        <Img fluid={item.fluid} alt={item.altText} />
        <CarouselCaption captionText={item.captionText} captionHeader={item.captionHeader} />
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
      className={props.className}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default ImageCarousel;
