import React from 'react'
import { Link } from 'gatsby'
import {Container, Row, Col} from 'reactstrap'
import Socials from './Socials'

const navPages = ["home", "events", "about", "blog", "music", "videos"]

const Footer = props => {
    return (
        <footer className="pt-3">
            <Container className="text-white container justify-content-center text-center">
                <Row className="py-3">
                    {navPages.map( page => {
                        if (page === "home") {
                            return (
                                <Col key={page} className="py-2">
                                    <Link className="text-white" to={`/`}>{page}</Link>
                                </Col>
                            )
                        } else {
                            return (
                                <Col className="py-2" key={page}>
                                    <Link className="text-white" to={`/${page}`}>{page}</Link>
                                </Col>
                            )
                        }}
                    )}
                </Row>
                <Row >
                    <Col>
                        <Socials className="text-decoration-none text-white" style={{width: "240px"}}/>
                    </Col>
                </Row>
                <Row className="pt-3">
                    <Col>
                        ©{new Date().getFullYear()}
                    </Col>
                </Row>
           </Container>    
        </footer>
     
    )
}

export default Footer


