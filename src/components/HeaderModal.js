import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Container, Row, Col} from 'reactstrap';
import { Link } from "gatsby"
import Image from "./Image"
import './fontLibrary.js'

const rowStyle = "h2 my-3"
const linkStyle = "text-dark"
const navPages = ["home", "events", "about", "blog", "music", "videos"]

const HeaderModal = (props) => {

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div className="text-center">
      <FontAwesomeIcon className="text-white" icon={"bars"} onClick={toggle} size="lg"/>
      <Modal isOpen={modal} toggle={toggle} className="full-modal">
        <ModalHeader toggle={toggle}>
        MENU
        </ModalHeader>
        <ModalBody>
          <Container fluid="true" className="full-modal text-center align-items-center justify-content-center">
              <Row className="mb-3" >
                  <Col></Col>
                  <Col className="col-6"><Image fileName="BBGM-logo-square.png" alt="BBGM-logo-square"/></Col>
                  <Col></Col>
              </Row>
              {navPages.map( page => {
                if (page === "home") {
                  return (
                    <Row className={rowStyle} key={page}>
                      <Col>
                          <a className={linkStyle} href={`localhost:8000/`}>{page.toUpperCase()}</a>
                      </Col>
                    </Row>
                  )
                } else {
                  return (
                    <Row className={rowStyle} key={page}>
                      <Col>
                        <Link className={linkStyle} onClick={toggle} to={`/${page}`}>{page.toUpperCase()}</Link>
                      </Col>
                    </Row>
                  ) 
                }})}
          </Container>
        </ModalBody>
        <ModalFooter>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default HeaderModal;