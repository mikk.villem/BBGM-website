import React, { useState } from 'react';
import { useStaticQuery, graphql, Link } from "gatsby"
import Img from "gatsby-image"
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container, Row, Col } from 'reactstrap';
import Socials from './Socials'

const links = [
    { href: '/', text: 'Home' },
    { href: '/events', text: 'Events' },
    { href: '/about', text: 'About' },
    { href: '/blog', text: 'Blog' },
    { href: '/music', text: 'Music' },
    { href: '/videos', text: 'Videos' },
  ];

const createNavItem = ({ href, text, className }) => (
<NavItem key={text} className="align-self-center">
    <NavLink href={href} className={className}>{text}</NavLink>
</NavItem>
);

const HeaderNav = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Container className="px-4 pt-1 pt-md-3 justify-content-center">
      <Row className="">
        <Col className="d-flex justify-content-center">
          <Link className="display-4 d-none d-md-block text-decoration-none text-dark pt-3" to='/'>Black Bread Gone Mad</Link>
        </Col>
      </Row>
        <Navbar color="light" light expand="md" className="justify-content-between justify-content-md-center">
          <NavbarBrand className="d-md-none" href="/"><LogoImage /></NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto mx-md-auto d-flex text-center" navbar>
              {links.slice(0, 3).map(createNavItem)}
              <NavItem >
                <NavLink href="/"><LogoImage /></NavLink>
              </NavItem>
              {links.slice(3).map(createNavItem)}
            </Nav>
          </Collapse>
        </Navbar>
      <Socials className="text-decoration-none text-dark" style={{width: "240px"}} />
    </Container>
  );
}

export default HeaderNav;

const LogoImage = () => {
    const data = useStaticQuery(graphql`
    query LogoImage {
        image: file(relativePath: {eq: "BBGM-logo-square.png"}) {
            childImageSharp {
              fixed(height: 50, width: 50) {
                ...GatsbyImageSharpFixed
              }
            }
            id
          }
    }
    `)
    return (
        <div className="p-1">
            <Img className="px-3 rounded-circle" fixed={data.image.childImageSharp.fixed} />
        </div>
    )
}