import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"


const Image = (props) => (
  <StaticQuery
    query={graphql`
      query {
        images: allFile (filter: {
          extension: {in: ["png", "jpg"]},
          sourceInstanceName: {eq: "images"}
        }){
          edges {
            node {
              relativePath
              name
              childImageSharp {
                sizes(maxWidth: 600) {
                  ...GatsbyImageSharpSizes
                }
                fluid{
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    `}

    render={(data) => {
      const image = data.images.edges.find(n => {
        return n.node.relativePath.includes(props.fileName);
      });
      if (!image) { return null; }
      
      //const imageSizes = image.node.childImageSharp.sizes;
      const imageFluid = image.node.childImageSharp.fluid;
      return (
        <div className={props.className}>
          <Img
            alt={props.alt}
            fluid={imageFluid}
            
          />
        </div>
      );
    }}
  />
)

export default Image

/* const Image = () => {
    const data = useStaticQuery(graphql`
    query Images {
        image: file(relativePath: {eq: "kontsert2-bw.jpg"}) {
            id
            childImageSharp {
              fluid{
                ...GatsbyImageSharpFluid
              }
            }
        }
    }
    `)

    console.log(data)
    return (
        <div className="p-1">
            <Img className="px-3" fluid={data.image.childImageSharp.fluid} />
        </div>
    )
} */