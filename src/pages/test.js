import React from "react"
import Layout from "../components/Layout"
import Image from "../components/Image"
import { Link } from 'gatsby'
import SEO from "../components/Seo"
import { Container, Row, Col } from 'reactstrap';

const bgStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  height: "200px"}
const bgStyleMusic = {
  backgroundPosition: 'center 25%',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  height: "150px"}
const bgStyleVideos = {
  backgroundPosition: 'center 40%',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  height: "100px"}

const colMobile = "my-2 text-white h1"
const flexCenter = "d-flex justify-content-center align-items-center"
const [events, about, blog, music, videos] = ["events", "about", "blog", "music", "videos"]

const IndexPage = () => (
  <Layout >
    <SEO title="Home" />
      <div >
      <Image fileName="BBGM_2018_Renee Altrov_huge.jpg" />
    </div>
    <Container >
      <Row xs="1" md="2" className="py-2 px-3 py-sm-0 px-sm-0 " >
      <Link className="text-decoration-none" to="/">
        <Col 
          className={[colMobile, events, flexCenter]}
          style={bgStyle}
        >
          EVENTS
        </Col>
      </Link>
      <Link className="text-decoration-none" to="/about">
        <Col 
          className={[colMobile, about, flexCenter]}
          style={bgStyleMusic} 
        >
          ABOUT
        </Col>
      </Link>
      <Link className="text-decoration-none" to="/blog">
        <Col  
          className={[colMobile, blog, flexCenter]}
          style={bgStyle}
        >
          BLOG
        </Col>
      </Link>
      <Link className="text-decoration-none" to="/music">
        <Col 
          className={[colMobile, music, flexCenter]}
          style={bgStyleMusic}
        >
          MUSIC
        </Col>
      </Link>
      <Link className="text-decoration-none" to="/videos">
        <Col 
          className={[colMobile, videos, flexCenter]}
          style={bgStyleVideos}
        >
          VIDEOS
        </Col>
      </Link>
      </Row>
    </Container>
  </Layout>
)

export default IndexPage
