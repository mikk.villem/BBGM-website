import React from "react"
import {Container,Row,Col, Button} from 'reactstrap'
import albumCover from '../images/ayibobo-cover.jpg'
import Layout from "../components/Layout"
import SEO from "../components/Seo"

const rowStyle="py-4 text-dark justify-content-center"

const Works = () => (
  <Layout>
    <SEO title="Music" />
    <Container className="text-dark text-center overflow-auto">
      <Row className={rowStyle}>
          <h1>Music for the soul</h1>
      </Row>  
      <Row className={rowStyle}>
        <Col sm="10" lg="8" >
          <a href="https://www.apollo.ee/black-bread-gone-mad-ayibobo-cd.html" target="blank">
            <img src={albumCover} alt="Ayibobo album cover" width={"70%"} />
          </a>
        </Col>
      </Row>
      <Row className={rowStyle}>
        <Col>
          <a href="https://www.apollo.ee/black-bread-gone-mad-ayibobo-cd.html" target="blank">
            <Button className="btn-outline-light" size="lg">Osta album</Button>
          </a>
        </Col>
      </Row>
      <Row className={rowStyle}>
        <Col>
          <iframe 
            src="https://open.spotify.com/embed/artist/1xGCdUjcoFCu9tosmcBMgL" 
            title="BBGM"
            width="300" 
            height="400" 
            frameborder="0" 
            allowtransparency="true" 
            allow="encrypted-media">
          </iframe>
        </Col>
      </Row>
    </Container>
  </Layout>
)

export default Works
