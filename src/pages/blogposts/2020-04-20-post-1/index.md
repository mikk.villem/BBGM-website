---
collection: "blog"
path: "/post-1"
date: May 12, 2020
title: "COVID-19 update"
author: "Mati"
image: "IMG_8402.jpg"
---

Black Bread Gone mad on proge-rebel folk 
                    ansambel Eestist. Kooseis esitab omaloomingut, 
                    mis saanud mõjutusi Eesti pärimusmuusikast, maailmamuusikast ja 
                    70ndate rokist. (Äge vintaažihõnguline bänd.) BBGM pöörased ja energiast pakatavad 
                    kontserdid näitavad kui piiranguteta ja piirideta võib muusika olla. Ansambli liikmed 
                    on ise suured 70ndate roki, afro-rütmide, Eesti pärimusmuusika ja Lähis-Ida muusikastiilide 
                    austajad. BBGM muusika sünnib soovist luua värskust ja põnevust pärimusmuusika skeenele.