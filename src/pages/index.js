import React from "react"
import Layout from "../components/Layout"
/* import Image from "../components/Image" */
import {useStaticQuery, graphql } from 'gatsby'
import {Container, Row, Col} from 'reactstrap'
import SEO from "../components/Seo"
import {Link} from 'gatsby'
import Img from "gatsby-image"



const IndexPage = () => (
<div>
    <Layout >
        <SEO title="Homepage"/>
        <Container className="px-4 px-xl-2 pb-4 home-container">
            <HomeMenuImages />
        </Container>
    </Layout>
</div>
)

export default IndexPage

const HomeMenuImages = () => {
        const data = useStaticQuery(graphql`
        query HomeImageGallery {
            menuImages: allFile(filter: {relativeDirectory: {eq: "menu"}}) {
                nodes {
                    childImageSharp {
                        id 
                        fluid{
                            ...GatsbyImageSharpFluid
                        }
                    }
                    name
                }
            }
        }
        `)

    const navPages = [
        {id: 0, page: "events"},
        {id: 1, page: "about"},
        {id: 2, page: "blog"},
        {id: 3, page: "music"},
        {id: 4, page: "videos"}]

    return (
        
        <Row xs="1" md="2" xl="3" className="pt-xl-4">
            {navPages.map( navPage => (
                <Link key={navPage.id} to={`/${navPage.page}`}>
                <Col className="pt-4 ">
                    <Img fluid={data.menuImages.nodes[navPage.id].childImageSharp.fluid} alt={navPage.page}/>
                    <Container className="h2 font-weight-bold centered text-white text-decoration-none">{navPage.page.toUpperCase()}</Container>
                </Col>
                </Link>
             ))}
        </Row>
    )
}

