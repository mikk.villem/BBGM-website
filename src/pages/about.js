import React from "react"
/* import { Link } from "gatsby" */
import {Container,Row,Col} from 'reactstrap'
import Layout from "../components/Layout"
import ImageCarousel from '../components/ImageCarousel'
import Image from "../components/Image"
import SEO from "../components/Seo"

const Works = () => (
  <Layout>
    <SEO title="About our band" />
    <Container className="text-dark overflow-auto justify-content-center">
      <Row >
        <Col className="h1 text-center pt-4 pb-4">
          About Us
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col xs="10" className="">
          <Image fileName="BBGM_Renee Altrov_2018_huge_bw.jpg" alt="band" className="pb-5" />
          <p>
        Black Bread Gone mad on proge-rebel folk 
        ansambel Eestist. Kooseis esitab omaloomingut, 
        mis saanud mõjutusi Eesti pärimusmuusikast, maailmamuusikast ja 
        70ndate rokist. (Äge vintaažihõnguline bänd.) BBGM pöörased ja energiast pakatavad 
        kontserdid näitavad kui piiranguteta ja piirideta võib muusika olla. Ansambli liikmed 
        on ise suured 70ndate roki, afro-rütmide, Eesti pärimusmuusika ja Lähis-Ida muusikastiilide 
        austajad. BBGM muusika sünnib soovist luua värskust ja põnevust pärimusmuusika skeenele.
        </p>
        <ImageCarousel className="py-4"/>
        </Col>
      </Row>
    </Container>
  </Layout>
)

export default Works
