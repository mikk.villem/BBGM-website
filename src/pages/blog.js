import React from "react"
import { Link, graphql } from "gatsby"
import {
  Container, Row, Col,
  Card, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';
import Image from '../components/Image'
import Layout from "../components/Layout"
import SEO from "../components/Seo"

const BlogPage = ( {data} ) => (
  <Layout>
    <SEO 
    title="Blog" 
    />
    <Container>
      <Row className="h1 justify-content-center py-4">
        Welcome to the blog
      </Row>
      <Row xs="1" md="2">
    {data.allMarkdownRemark.edges.map(post => (
      <Col className="p-2" key={post.node.id}>
        <Card>
          <Image  className="px-3 pt-3" fileName={post.node.frontmatter.image} alt="Band on railway tracks" />
          <CardBody>
            <CardTitle className="h3">{post.node.frontmatter.title}</CardTitle>
            <CardSubtitle className="py-2">Posted by {post.node.frontmatter.author} on {post.node.frontmatter.date}</CardSubtitle>
            <Link to={post.node.frontmatter.path}>
              <Button outline >Read more</Button>
            </Link>
          </CardBody>
        </Card>
      </Col>
    ))}
    </Row>
    </Container>
  </Layout>
)

export const pageQuery = graphql`
    query BlogIndexQuery 
        {
            allMarkdownRemark(filter: {frontmatter: {collection: {nin: "events"}}}) {
              edges{
                node {
                  id
                  frontmatter {
                    path
                    date
                    title
                    author
                    image
                  }
                }
              }
            }
        }
`

export default BlogPage
