import React from 'react';
import YouTube from 'react-youtube';
import SEO from "../components/Seo"
import Layout from "../components/Layout"
import {
 Container,
 Row, 
 Col
} from 'reactstrap';
import {videos} from '../videos/youtube';

const VideoPage = () => {

  const opts = {
    height: '270',
    width: '480',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 0,
    },
  };

  const onReady = (event) => {
    // access to player in all event handlers via event.target 
    event.target.pauseVideo();
  }

  return (
    <Layout >
      <SEO title="Videos"/>      
      <Container>
      <Row className="h1 justify-content-center text-dark py-4">
          Watch us in action
      </Row>
      <Row className="justify-content-around " xs="1" lg="2">
          
        {videos.map(video =>
        <Col key={video.link} className="video-container my-3" xs="10" lg="5">
           <YouTube  className="video-player" videoId={video.link} opts={opts} onReady={() => onReady}/>
        </Col>
        )}
          
        </Row> 
      </Container>
  </Layout>
  )
}

export default VideoPage
