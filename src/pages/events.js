import React from "react"
import { graphql } from "gatsby"
import {
  Container, Row, Col,
  Card, CardBody,
  CardTitle, CardSubtitle, 
} from 'reactstrap';
import Image from '../components/Image'
import Layout from "../components/Layout"
import SEO from "../components/Seo"

const EventsPage = ( {data} ) => (
  <Layout>
    <SEO title="Events" />
    <Container>
      <Row className="h1 justify-content-center py-4">
        Upcoming events
      </Row>
      <Row xs="1" md="2" className="justify-content-center">
    {data.allMarkdownRemark.edges.map(post => (
      <Col className="p-2" key={post.node.id}>
      <a href={post.node.frontmatter.outsideLink} target="blank" className="text-decoration-none text-dark">
        <Card>
          <Image  className="px-3 pt-3" fileName={post.node.frontmatter.image} alt="Band on railway tracks" />
          <CardBody>
            <CardTitle className="h3">{post.node.frontmatter.title}</CardTitle>
            <CardSubtitle className="py-2">@ {post.node.frontmatter.location} on {post.node.frontmatter.date}</CardSubtitle>
          </CardBody>
        </Card>
        </a>
      </Col>
    ))}
    </Row>
    </Container>
  </Layout>
)

export const pageQuery = graphql`
    query EventsIndexQuery 
        {
            allMarkdownRemark(filter: {frontmatter: {collection: {eq: "events"}}}) {
              edges{
                node {
                  id
                  frontmatter {
                    date
                    title
                    location
                    outsideLink
                    image
                  }
                }
              }
            }
        }
`

export default EventsPage