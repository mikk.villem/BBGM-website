import React from 'react';
import { Link, graphql } from "gatsby"
import {Container,Row,Col} from 'reactstrap'
import Image from "../components/Image"
import SEO from "../components/Seo"
import Layout from "../components/Layout"

const Template = ({data}) => {
    const post = data.markdownRemark;

    return (
        <Layout>
            <SEO title="about Black Bread Gone Mad" />
            <Container className="text-dark overflow-auto justify-content-center">
                <Row className="flex-column align-items-center">
                    <Col className="h1 text-center pt-4 pb-4">
                        {post.frontmatter.title}
                    </Col >
                    <Col className="h4 text-center pb-4">
                        Posted by {post.frontmatter.author} on {post.frontmatter.date}
                    </Col>
                    <Col xs="10" md="8" className="">
                        <Image fileName={post.frontmatter.image} alt="band" className="pb-5" />
                    </Col>
                    <Col xs="10" dangerouslySetInnerHTML= {{ __html: post.html }} /> 
                    <Col className="text-center pb-4">
                        <Link to="/blog">Tagasi</Link>
                    </Col>
                </Row>
                
            </Container>
        </Layout>
)
      
}

export default Template

export const postQuery = graphql`
    query PostyPath($path: String!){
        markdownRemark(frontmatter: { path: {eq: $path}}){
            html
            frontmatter {
                path
                title
                author
                date
                image
            }
        }
    }
`

